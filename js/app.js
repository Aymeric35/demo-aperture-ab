const images = document.querySelectorAll(".slider__img");
const sliderCount = images.length;
let currentCount = 0;
const dots = [];
let autoSlider = setInterval(sliderNext, 2500);

function initSlider() {
    const prev = document.getElementById("sliderPrev");
    const next = document.getElementById("sliderNext");
    const slider = document.getElementById("dotsContainer");

    images.forEach((_, index) => {
        dots.push(document.createElement("div"));
        dots[index].classList.add("slider__dots");
        dots[index].classList.add("dot-" + index);
        slider.appendChild(dots[index]);
    })

    dots[currentCount].classList.toggle("is-active");
    prev.addEventListener("click", sliderPrev);
    next.addEventListener("click", sliderNext);
    dots.forEach(dot => dot.addEventListener("click", e => {
        sliderDots(e)
    }));
}

function slideMove(direction) {
    images[currentCount].classList.toggle("is-active");
    dots[currentCount].classList.toggle("is-active")
    if (direction === "prev") currentCount === 0 ? currentCount = sliderCount - 1 : currentCount--;
    if (direction === "next") currentCount >= sliderCount - 1 ? currentCount = 0 : currentCount++;
    images[currentCount].classList.toggle("is-active");
    dots[currentCount].classList.toggle("is-active");
}

function sliderPrev() {
    slideMove("prev");
    clearInterval(autoSlider);
    autoSlider = setInterval(sliderNext, 3500);
}

function sliderNext() {
    slideMove("next");
    clearInterval(autoSlider);
    autoSlider = setInterval(sliderNext, 3500);
}

function sliderDots(e) {
    dots[currentCount].classList.toggle("is-active");
    images[currentCount].classList.toggle("is-active");
    currentCount = e.target.className.slice(-1);
    e.target.classList.toggle("is-active");
    images[currentCount].classList.toggle("is-active");
    clearInterval(autoSlider);
    autoSlider = setInterval(sliderNext, 3500);
}

initSlider();